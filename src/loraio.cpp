#include "loraio.h"

void loraio::radio_set(uint8_t rcv_id, uint8_t snd_id){
    hdr.receiver_id = rcv_id;
    hdr.sender_id = snd_id;
}

void loraio::radio_send(lora dev, radio_msg_sensor_frame msgf){
    hdr.msg_id++;
    hdr.payload_len = sizeof(radio_msg_sensor_frame);
    lora_begin_packet(&dev, false);
    lora_write_data(&dev, (const uint8_t*)&hdr, sizeof(hdr));
    lora_write_data(&dev, (const uint8_t*)&msgf, sizeof(msgf));
    lora_end_packet(&dev, false);

    lora_receive(&dev, 0);
}
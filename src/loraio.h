#include "lora-mbed.h"

typedef struct
{
	uint8_t receiver_id;           // Receiver address
	uint8_t sender_id;             // Sender address
	uint8_t msg_id;                // Message ID
	uint8_t payload_len;           // Message payload length
} radio_layer_msg_header;

typedef struct
{
	uint8_t alarmSet;
	float temp, hum;
} radio_msg_sensor_frame;

#ifndef loraio_H
#define loraio_H

class loraio{
	public:
	bool packet_ready = false;
	radio_layer_msg_header hdr;
	radio_layer_msg_header rcvhdr;
	radio_msg_sensor_frame rcvframe;

	void radio_set(uint8_t rcv_id, uint8_t snd_id);
	
	void radio_send(lora dev, radio_msg_sensor_frame msgf);
};

#endif
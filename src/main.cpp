#include "mbed.h"
#include "lora-mbed.h"
#include "loraio.h"
#include "TCPSocket.h"
#include "ESP8266Interface.h"
#include "MQTTClientMbedOs.h"
#include "secrets.h"
#include <cstdint>

const uint8_t sender_id = 0x01;               //this router
const uint8_t receiver_id = 0x10;             //sensor    

SPI spi(PB_5, PB_4, PB_3);
DigitalOut nss(PB_10);
DigitalInOut rst(PA_8);
InterruptIn dio0(PA_9);

lora dev;
radio_msg_sensor_frame msg;
loraio loraio_io;

ESP8266Interface wifi(PA_11, PA_12);        //to rx, tx

static void on_rx_done(void *ctx, int packet_size){
    lora *const dev = (lora *const) ctx;
    
    uint8_t *p = (uint8_t*)&loraio_io.rcvframe;
    size_t i = 0;

    if(packet_size == 0)    goto err;

    loraio_io.rcvhdr.receiver_id = lora_read(dev);
    loraio_io.rcvhdr.sender_id = lora_read(dev);
    loraio_io.rcvhdr.msg_id = lora_read(dev);
    loraio_io.rcvhdr.payload_len = lora_read(dev);

    if((loraio_io.rcvhdr.receiver_id != loraio_io.hdr.sender_id) | (loraio_io.rcvhdr.payload_len != sizeof(radio_msg_sensor_frame)))      goto err;

    while((lora_available(dev)) & (i < loraio_io.rcvhdr.payload_len)){
        *(p+i) = (uint8_t)lora_read(dev);
        i++;
    }
    loraio_io.packet_ready = true;
    return;

    err:    lora_receive(dev, 0);
}

int main(){   
    dev.frequency = 433E6;
    dev.on_receive = NULL;
    dev.on_tx_done = NULL;
    loraio_io.radio_set(receiver_id, sender_id);
    
    lora_mbed mbed_dev;
    mbed_dev.spi = &spi;
    mbed_dev.nss = &nss;
    mbed_dev.reset = &rst;
    mbed_dev.dio0 = &dio0;
    lora_mbed_init(&dev, &mbed_dev);
    lora_on_receive(&dev, on_rx_done);

    uint8_t rc = 0;
    while(rc == 0){
        rc = wifi.connect(SSID, PASS);
        thread_sleep_for(500);
    }


    char message_buffer[256];
    TCPSocket socket;
    socket.open(&wifi);
    SocketAddress MQTTBroker;
    wifi.gethostbyname(MQBrokerHostname, &MQTTBroker);
    MQTTBroker.set_port(MQBrokerPort);
    rc = socket.connect(MQTTBroker);
    if(rc != 0) return -1;
    MQTTClient client(&socket);
    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
    data.MQTTVersion = 3;
    data.clientID.cstring = MQClient;
    data.username.cstring = MQUser;
    data.password.cstring = MQToken;

    rc = client.connect(data);
    if(rc != 0) return -1;
    
    MQTT::Message message;
    message.qos = MQTT::QOS0;
    message.retained = false;
    message.dup = false;
    string topicString;

    while(1){
        if(loraio_io.packet_ready){
            if(loraio_io.rcvframe.alarmSet != 0){
                sprintf(message_buffer, "[{\"variable\":\"Allarme\",\"value\":",loraio_io.rcvframe.alarmSet, "}]");
                message.payload = (void *)message_buffer;
                message.payloadlen = strlen(message_buffer);
                topicString = to_string(loraio_io.rcvhdr.sender_id);
                client.publish(topicString.c_str(), message);
            }
            else{
                sprintf(message_buffer, "[{\"variable\":\"Temperatura\",\"value\":%2.2f", loraio_io.rcvframe.temp, "},{\"variable\":\"Umidita\",\"value\":%2.2f", loraio_io.rcvframe.hum, "}]");
                message.payload = (void *)message_buffer;
                message.payloadlen = strlen(message_buffer);
                topicString = to_string(loraio_io.rcvhdr.sender_id);
                client.publish(topicString.c_str(), message);
            }
        }
        thread_sleep_for(500);
    }
}